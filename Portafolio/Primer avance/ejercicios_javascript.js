/*let welcome = "Hello Wolrd"
console.log(welcome)


let array = [1,2,3,4,5,"Yun", "Bryan", true, false, 2.1, 5.4]
let array2 = [1,2,3,4,5,"Yun", "Bryan", true, false, 2.1, 5.4]

let obj = {arr1: array, arr2:array2};
console.log(Object.keys(obj))
console.log("//////")


for (let i = 0; i < array.length; i++) {
    console.log(array[i])
}

for (let i of array) {
    console.log(i)
}

console.log("//////")
console.log(Object.values(obj))

Object.entries(obj).forEach(([key, value]) => {
    console.log(`${key} : ${value}`);
});
    
console.log("While")

var i = 0;
while(i<50) {
    console.log(i);
    i+=5;
}

console.log("Do While")


var k = 0;
do {
    console.log(k);
    k+=5;
} while(k<50);

console.log("For in")*/

let z = 10;
let x = 20;

if (x<z) { 
    console.log("z es menor que x")
} else {
    console.log("x es menor que z")
}

let variable = (x != z) ?  console.log(`El mayor es x (${x})`) : console.log(`El mayor es z (${z})`);

switch(x===z) {
    case true:
        console.log("x es igual a z")
        break;
    case false:
        console.log("x es diferente a z")
}


var uno ="hello";

function func() {
    var uno = "world"
    console.log(uno);
}


function prism(l,w, h) {
    return l * w * h;
}

function prism2(l) {
    return function(w) {
        return function(h) {
            return l * w * h;
        }
    }
}


console.log(prism(2,3,5))
console.log(prism2(2)(3)(5))

const foo = (function() {
    console.log("Funcion anonima");
    return 5
 })();
 

 var namedSum = function sum(a, b) {
    return a + b;
 } 

 var AnonSum = function(a, b) {
    return a + b;
 } 

 console.log(namedSum(1, 3))
 console.log(AnonSum(1, 3))


/*
 var say = function(times) {
    if (times > 0) {
        console.log("Hello");
        say(times -1);
    }
 }

 var saysay = say;

 say = "Oops!";

 saysay(5);
*/

 var say = function say(times) {

    say = undefined; 

    if (times > 0) {
        console.log("Hello");
        say(times -1);
    }
  };
  

  var saysay = say;
  
  var oops = "Oops!";
  
  saysay(5);
  console.log(oops);
  
  var func = (msg="Bryan") => {
    console.log(msg)
  }


    /*const apiUrl = 'https://api.stackexchange.com/2.2/questions/featured?order=desc&sort=activity&site=stackoverflow';

    fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
        for (var item of data.items) {
        console.log("title: " + item.title);
        console.log("score: " + item.score);
        console.log("owner: " + item.owner.display_name);
        console.log("link: " + item.link);
        console.log("answer Count: " + item.answer_count);
        console.log(" ");
        }
    });


   
    const apiUrl2 = 'https://jsonplaceholder.typicode.com/users';

    fetch(apiUrl2).then(response => response.json())
    .then(elements => {
        elements.forEach(el => {
        console.log("ID: " + el.username);
        });
    });

    fetch(apiUrl2, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: 'Santiago Alejo',
            username: 'Santiago',
            email: 'sant123@email.com'
        })
    }).then(response => response.json())
     .then(response => {
         console.log(response);
     })
    
     const apiUrl3 = 'https://jsonplaceholder.typicode.com/posts';

     fetch(apiUrl3).then(response => response.json())
     .then(elements => {
         elements.forEach(el => {
            console.log("Title: " + el.title);
         });
     });

     fetch(apiUrl3, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            title: 'Mi post',
        })
    }).then(response => response.json())
     .then(response => {
         console.log(response);
     })*/

