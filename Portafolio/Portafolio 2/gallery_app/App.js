// App.js
import React, { useState } from 'react';
import { Button, View } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import ImageViewer from './components/ImageViewer'; // Asegúrate de importar tu componente ImageViewer

export default function App() {
 const [selectedImage, setSelectedImage] = useState(null);

 const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setSelectedImage(result.uri);
    }
 };

 return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button title="Seleccionar imagen" onPress={pickImage} />
      {selectedImage && <ImageViewer imageUri={selectedImage} />}
    </View>
 );
}
