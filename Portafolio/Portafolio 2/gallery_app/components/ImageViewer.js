// components/ImageViewer.js
import React from 'react';
import { Image, StyleSheet } from 'react-native';

export default function ImageViewer({ imageUri }) {
 return (
    <Image source={{ uri: imageUri }} style={styles.image} />
 );
}

const styles = StyleSheet.create({
 image: {
    width: 200,
    height: 200,
    resizeMode: 'cover',
 },
});
