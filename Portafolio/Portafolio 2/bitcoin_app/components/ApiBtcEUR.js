import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { useState, useEffect } from 'react';

const apiEUR = "https://api.coindesk.com/v1/bpi/currentprice/EUR.json";

export default function ApiBtcEUR() {
 const [response, setResponse] = useState(null);
 const [isLoading, setIsLoading] = useState(true);
 const [error, setError] = useState(null);

 useEffect(() => {
  const fetchData = async () => {
     try {
       const resEUR = await fetch(apiEUR);
       const dataEUR = await resEUR.json();
       setIsLoading(false);
       setResponse(dataEUR.bpi.EUR);
     } catch (error) {
       setIsLoading(false);
       setError(error);
     }
  };
 
  fetchData();
 }, []);

 const getContent = () => {
    if (isLoading) {
      return (
        <View>
          <Text style={styles.textSize}>Loading Data...</Text>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    if (error) {
      return (
        <Text style={styles.textSize}>Error: {error.message}</Text>
      );
    }
    return (
      <View style={styles.container}>
        <Text style={styles.textSize}>BTC to EUR: €{response.eur.rate}</Text>
        <StatusBar style="auto" />
      </View>
    );
 }

 return (
    <View style={styles.container}>
      {getContent()}
    </View>
 );
}

const styles = StyleSheet.create({
 container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
 },
 textSize: {
    fontSize: 36,
 },
});
