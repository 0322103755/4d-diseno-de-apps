import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { useState, useEffect } from 'react';

const apiGBL = "https://api.coindesk.com/v1/bpi/currentprice/GBL.json";

export default function ApiBtcGBL() {
 const [response, setResponse] = useState(null);
 const [isLoading, setIsLoading] = useState(true);
 const [error, setError] = useState(null);

 useEffect(() => {
  const fetchData = async () => {
     try {
       const resGBL = await fetch(apiGBL);
       const dataGBL = await resGBL.json();
       setIsLoading(false);
       setResponse(dataGBL.bpi.GBL);
     } catch (error) {
       setIsLoading(false);
       setError(error);
     }
  };
 
  fetchData();
 }, []);

 const getContent = () => {
    if (isLoading) {
      return (
        <View>
          <Text style={styles.textSize}>Loading Data...</Text>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    if (error) {
      return (
        <Text style={styles.textSize}>Error: {error.message}</Text>
      );
    }
    return (
      <View style={styles.container}>
        <Text style={styles.textSize}>BTC to GBL: €{response.eur.rate}</Text>
        <StatusBar style="auto" />
      </View>
    );
 }

 return (
    <View style={styles.container}>
      {getContent()}
    </View>
 );
}

const styles = StyleSheet.create({
 container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
 },
 textSize: {
    fontSize: 36,
 },
});
