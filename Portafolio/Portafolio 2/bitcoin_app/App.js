import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import ApiBitUSD from './components/ApiBtcUSD';
import ApiBtcEUR from './components/ApiBtcEUR';
import ApiBtcGBL from './components/ApiBtcGBL';

export default function App() {
  return (
    <View style={styles.container}>
      <ApiBitUSD/>
      <ApiBtcEUR/>
      <ApiBtcGBL/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
