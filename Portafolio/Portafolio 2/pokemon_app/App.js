// App.js
import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import PokemonList from './components/PokemonList'; 

const App = () => {

 const [pokemonList, setPokemonList] = useState([]);

 useEffect(() => {
    fetch('https://pokeapi.co/api/v2/pokemon?limit=384')
      .then(response => response.json())
      .then(data => {
        let promisesArray = data.results.map(result => {
          return fetch(result.url).then(response => response.json());
        });
        return Promise.all(promisesArray);
      })
      .then(pokemonData => {
        setPokemonList(pokemonData);
      })
      .catch(error => console.error(error));
 }, []);

 return (
    <View style={styles.container}>
      <PokemonList pokemonList={pokemonList} />
    </View>
 );
};

const styles = StyleSheet.create({
 container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
 },
});

export default App;
