// PokemonList.js
import React from 'react';
import { View, FlatList, StyleSheet, Image, Text } from 'react-native';
import { ScrollView } from 'react-native-web';

const PokemonList = ({ pokemonList }) => {
   const getCardColor = (type) => {
      switch (type) {
         case 'fire':
           return '#EE8130';
         case 'water':
           return '#6390F0';
         case 'grass':
           return '#7AC74C';
         case 'electric':
           return '#F7D02C'; 
         case 'psychic':
           return '#F95587'; 
         case 'bug':
           return '#A6B91A'; 
         case 'flying':
           return '#A98FF3'; 
         case 'normal':
           return '#A8A77A'; 
         case 'poison':
           return '#A33EA1'; 
         case 'ground':
           return '#E2BF65'; 
         case 'fighting':
           return '#C22E28'; 
         case 'ice':
           return '#96D9D6'; 
         case 'ghost':
           return '#735797'; 
         case 'dragon':
           return '#6F35FC'; 
         case 'dark':
           return '#705746';
         case 'steel':
           return '#B7B7CE';
         case 'fairy':
           return '#D685AD';
         default:
           return '#FFFFFF'; 
      }
     };
     

 const renderItem = ({ item }) => {
    const cardColor = getCardColor(item.types[0].type.name); 
    const pokemonType = item.types.map(type => type.type.name).join(', ');

    return (
      <View style={[styles.card, { backgroundColor: cardColor }]}>
        <Image source={{ uri: item.sprites.front_default }} style={styles.image} />
        <Text style={styles.name}>{item.name}</Text>
        <Text style={styles.type}>{pokemonType}</Text>
      </View>
    );
 };

 return (
      <View style={styles.container}>
         <FlatList
         data={pokemonList}
         renderItem={renderItem}
         keyExtractor={item => item.name}
         numColumns={3}
         />
      </View>

 );
};

const styles = StyleSheet.create({
 container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
 },
 card: {
    padding: 10,
    margin: 10,
    borderRadius: 10,
    backgroundColor: '#FFF',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
 },
 image: {
    width: 100,
    height: 100,
 },
 name: {
    fontSize: 18,
    fontWeight: 'bold',
 },
 type: {
    fontSize: 14,
    color: '#666',
 },
});

export default PokemonList;
