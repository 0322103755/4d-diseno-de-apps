module.exports = ({ env }) => ({
    email: {
      config: {
        provider: 'sendgrid',
        providerOptions: {
          apiKey: env('SENDGRID_API_KEY'),
        },
        settings: {
          defaultFrom: 'joseph.thecrack117@gmail.com',
          defaultReplyTo: 'joseph.thecrack117@gmail.com',
        },
      },
    },
  });