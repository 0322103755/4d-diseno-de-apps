'use strict';

/**
 * estado-parada controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::estado-parada.estado-parada');
