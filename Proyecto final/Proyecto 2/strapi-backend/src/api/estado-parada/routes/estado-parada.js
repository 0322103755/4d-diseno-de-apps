'use strict';

/**
 * estado-parada router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::estado-parada.estado-parada');
