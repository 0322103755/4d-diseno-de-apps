'use strict';

/**
 * modelo-vehiculo controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::modelo-vehiculo.modelo-vehiculo');
