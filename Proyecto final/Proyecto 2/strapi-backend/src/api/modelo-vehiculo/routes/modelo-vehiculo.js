'use strict';

/**
 * modelo-vehiculo router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::modelo-vehiculo.modelo-vehiculo');
