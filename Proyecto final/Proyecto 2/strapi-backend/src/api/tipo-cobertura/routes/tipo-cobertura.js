'use strict';

/**
 * tipo-cobertura router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::tipo-cobertura.tipo-cobertura');
