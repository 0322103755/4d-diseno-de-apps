'use strict';

/**
 * tipo-cobertura controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::tipo-cobertura.tipo-cobertura');
