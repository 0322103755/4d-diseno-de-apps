'use strict';

/**
 * tipo-cobertura service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::tipo-cobertura.tipo-cobertura');
