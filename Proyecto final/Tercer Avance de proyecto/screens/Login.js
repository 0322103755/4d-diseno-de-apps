//LISTO

import React, { useState, useRef, useEffect } from 'react';
import { View, TextInput, Button, Text, StyleSheet, Dimensions, Animated, TouchableOpacity, Image, Easing } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { LinearGradient } from 'expo-linear-gradient';
import logo from '../assets/pics/logo-tt.png';
import CustomInput from '../components/CustomInput';

const { width, height } = Dimensions.get('window');

const Login = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const ip = '34.94.10.37';

  const fadeAnim = useRef(new Animated.Value(0)).current;
  const scaleAnim = useRef(new Animated.Value(1)).current;
  const scrollAnim = useRef(new Animated.Value(0)).current;
  const rotateAnim = useRef(new Animated.Value(0)).current;

  const toggleSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  useEffect(() => {
    Animated.sequence([
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }),
      Animated.timing(scaleAnim, {
        toValue: 1.2,
        duration: 200,
        useNativeDriver: true
      }),
      Animated.timing(scaleAnim, {
        toValue: 1,
        duration: 200,
        useNativeDriver: true
      }),
      Animated.timing(rotateAnim, {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear,
        useNativeDriver: true
      })
    ]).start();
  }, []);

  const handleLogin = async () => {
    try {
      const response = await fetch(`http://${ip}:1337/api/auth/local`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          identifier: email,
          password: password,
        }),
      });
      const data = await response.json();
      if (response.status === 200) {
        navigation.navigate('Main', {
          screen: 'Profile',
          params: { token: data.jwt }
        });
        navigation.navigate('Cambio', { token: data.jwt});
        navigation.navigate('Car', { token: data.jwt});
        navigation.navigate('Cargas', { token: data.jwt});
        navigation.navigate('Map', { token: data.jwt });
      } else if (response.status === 400 || response.status === 401) {
        setError('Credenciales inválidas');
      } else {
        setError('Hubo un error al iniciar sesión');
      }
    } catch (error) {
      console.error('Error:', error);
      setError('Hubo un error al iniciar sesión');
    }
  };

  const handleForgotPassword = () => {
    navigation.navigate('Forgot');
  };

  return (
    <LinearGradient colors={['#7b92b2', '#fff']} style={styles.container}>
      <View style={styles.containerSVG}>
        <Image source={logo} style={{ width: '80%', height: 324, marginTop:90 }} resizeMode="cover" />
        
      </View>
      <View style={styles.contentContainer}>
        <Animated.Text style={[styles.titulo, { transform: [{ scale: scaleAnim }] }]}>TrekTrakr</Animated.Text>
        <Animated.Text style={[styles.subTitle, { transform: [{ scale: scaleAnim }] }]}>Bienvenido, inicia sesión para continuar.</Animated.Text>
        <TextInput
          placeholder="Ingresa tu correo"
          style={styles.textInput}
          onChangeText={setEmail}
          value={email}
        />
        <CustomInput
          value={password}
          onChangeText={setPassword}
          secureTextEntry={secureTextEntry}
          toggleSecureTextEntry={toggleSecureTextEntry}
        />
        {error ? <Text style={styles.errorMessage}>{error}</Text> : null}
        <TouchableOpacity onPress={handleLogin} activeOpacity={0.8}>
          <Animated.View style={[styles.loginButton, { transform: [{ scale: scaleAnim }] }]}>
            <Text style={styles.loginButtonText}>Login</Text>
          </Animated.View>
        </TouchableOpacity>
        <TouchableOpacity onPress={handleForgotPassword}>
          <Text style={[styles.forgotPassword, { color: 'blue' }]}>¿Olvidaste tu contraseña?</Text>
        </TouchableOpacity>
        <Text style={styles.forgotPassword}>¿No tienes cuenta?</Text>
        <StatusBar style="auto" />
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerSVG: {
    width: width,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 50,
  },
  titulo: {
    fontSize: 80,
    color: '#7b92b2',
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 20,
    color: 'gray',
    paddingBottom: 30
  },
  textInput: {
    padding: 10,
    paddingStart: 30,
    width: '80%',
    height: 50,
    marginTop: 10,
    borderRadius: 30,
    backgroundColor: '#fff',
  },
  forgotPassword: {
    fontSize: 14,
    color: 'gray',
    marginTop: 10,
  },
  errorMessage: {
    fontSize: 14,
    color: 'red',
    marginTop: 20,
  },
  loginButton: {
    backgroundColor: "#67cba0",
    borderRadius: 30,
    padding: 10,
    paddingHorizontal: 50,
    marginTop: 20,
    marginBottom: 20,
  },
  loginButtonText: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
  },
});

export default Login;
