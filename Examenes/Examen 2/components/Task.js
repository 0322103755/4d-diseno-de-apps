import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { TouchableOpacity } from 'react-native';
const Task = ({id, title, user, completed}) => {
  return (
    <View style={styles.item}>
        <View style={styles.itemLeft}>
            <TouchableOpacity style={completed? styles.squareCompleted :styles.square}><Text>{id}</Text></TouchableOpacity>
            <View style={styles.itemInfo}>
                {title && <Text style={completed? styles.itemTextCompleted : styles.itemText}>{title}</Text>}
                {user && <Text style={styles.itemUser}>Created by user: {user}</Text>}
                {(!user && !title) && (completed ? <Text style={styles.statusItem}>Status: Resolved </Text> : <Text style={styles.itemText}>Status: Unresolved</Text>)}
            </View>
            <Text style={styles.itemUser}>{completed}</Text>
        </View>
        <View style={completed ? styles.circularCompleted : styles.circular}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  itemUser: {
    padding: 4,
    color: "#55BCF6",
    fontSize: 14,
  },
  itemInfo: {
    display: "flex",
    flexDirection: "column",
    maxWidth: "90%",
  },
  statusItem: {
    fontSize: 14,
  },
  itemLeft: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
  },
  square: {
    width: 26,
    height: 26,
    backgroundColor: "#55BCF6",
    opacity: 0.8,
    borderRadius: 5,
    marginRight: 15,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontWeight: "bold"
  },

  squareCompleted: {
    width: 26,
    height: 26,
    backgroundColor: "#55BCF6",
    opacity: 0.4,
    borderRadius: 5,
    marginRight: 15,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontWeight: "bold"
  },
  itemText: {
    fontWeight: "bold",
    fontSize: 14,
  },
  itemTextCompleted: {
    fontSize: 14,
    textDecorationLine: "line-through",
    textDecorationColor: "#55bcf6",
  },
  item: {
    backgroundColor: "#FFF",
    padding: 15,
    borderRadius: 10,
    flexDirection:"row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  circular: {
    width: 12,
    height: 12,
    borderColor: "#55bcf6",
    borderWidth: 2,
    borderRadius: 5,
  },

  circular: {
    width: 14,
    height: 14,
    borderColor: "#55bcf6",
    borderWidth: 2,
    borderRadius: 5,
    color: "#55bcf6",
  },
  circularCompleted: {
    width: 14,
    height: 14,
    borderColor: "#55bcf6",
    borderWidth: 2,
    borderRadius: 5,
    backgroundColor: "#55bcf6",
  }
});


export default Task;