import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import Task from "./components/Task"
import { useEffect, useState } from 'react';
import { Picker } from '@react-native-picker/picker';

export default function App() {
  const [tasks, setTasks] = useState([]);
  const [viewOption, setViewOption] = useState('IDs');
 
  useEffect(() => {
     fetch('https://jsonplaceholder.typicode.com/todos')
       .then(response => response.json())
       .then(data => setTasks(data));
  }, []);
 
  const VIEW_OPTION_MAP = {
    'IDs': task => ({ id: task.id, completed: task.completed }),
    'IDs and Titles': task => ({ id: task.id, title: task.title, completed: task.completed }),
    'Unresolved (ID and Title)': task => !task.completed && ({ id: task.id, title: task.title, completed: task.completed }),
    'Resolved (ID and Title)': task => task.completed && ({ id: task.id, title: task.title, completed: task.completed }),
    'IDs and userID': task => ({ id: task.id, user: task.userId, completed: task.completed }),
    'Unresolved (ID and userID)': task => !task.completed && ({ id: task.id, user: task.userId, completed: task.completed }),
    'Resolved (ID and userID)': task => task.completed && ({ id: task.id, user: task.userId, completed: task.completed }),
   };

   const filteredTasks = tasks
   .filter(task => {
      if (viewOption.includes('Unresolved')) {
        return !task.completed;
      } else if (viewOption.includes('Resolved')) {
        return task.completed;
      }
      return true; 
   })
   .map(task => VIEW_OPTION_MAP[viewOption](task));
  
  
 
  return (
    <ScrollView style={styles.container}>
    <View style={styles.tasksWrapper}>
      <Text style={styles.sectionTitle}>NFL tasks</Text>
      <Picker
        selectedValue={viewOption}
        style={styles.picker}
        onValueChange={(itemValue) => setViewOption(itemValue)}
      >
        {['IDs', 'IDs and Titles', 'Unresolved (ID and Title)', 'Resolved (ID and Title)', 'IDs and userID', 'Unresolved (ID and userID)', 'Resolved (ID and userID)'].map((name) => (
          <Picker.Item key={name} label={name} value={name} />
        ))}
      </Picker>
      <View>
        {filteredTasks.map(task => (
          <Task
            key={task.id}
            {...task}
          />
        ))}
      </View>
    </View>
  </ScrollView>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
    paddingHorizontal: 30
  },
  tasksWrapper: {
    paddingTop: 80,
    paddingButtom: 20,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "bold",
    paddingBottom: 10,
  },
  items: {
    marginTop:30,
  },
  filterButtons: {
    paddingVertical: 10,
    flexDirection: 'row',
    flexWrap: 'wrap', 
    justifyContent: 'space-between', 
 },
 filterButtonContainer: {
    width: '48%', 
    marginBottom: 10, 
 },
 filterButton: {
    padding: 10,
    backgroundColor: '#55BCF6',
    borderRadius: 5,
 },
 filterButtonText: {
    color: '#FFF',
 },
 picker: {
  height: 50,
  width: '100%',
  backgroundColor: '#fff', 
  borderColor: '#000',
  borderWidth: 1,
  borderRadius: 10, 
  padding: 10, 
  marginVertical: 20
},
});